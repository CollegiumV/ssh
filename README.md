ssh
===
Configures sshd.

Supported Operating Systems
---------------------------
* CentOS 7
* Void Linux

Requirements
------------
No Requirements

Role Variables
--------------
```
---
ssh_firewall_services:
  - name: ssh
    description: Secure Shell
    priority: 1
    zones: '{{ ssh_firewall_zones }}'
    rules:
      - protocol: tcp
        port: '{{ ssh_port }}'
```

Role Defaults
-------------
```
---
ssh_port: 22
ssh_firewall_zones:
  - '{{ firewall_default_zone }}'
ssh_family: any
ssh_listen_addresses:
  - 0.0.0.0
  - '::'
ssh_hostkeys:
  - rsa
  - ed25519
ssh_syslogfacility: AUTH
ssh_loglevel: INFO
ssh_rootlogin: 'no'
ssh_passauth: 'yes'
ssh_keyauth: 'yes'
ssh_usepam: 'yes'
ssh_methods:
  - publickey
# ssh_authorized_command: <undefined>
# ssh_authorized_file: <undefined>
ssh_authorized_command_user: _sshd_keyuser
ssh_banner: |
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              University of Texas at Dallas - Collegium V
  Pursuant to Texas Administrative Code 202:
  (1) Unauthorized use is prohibited;
  (2) Usage may be subject to security testing and monitoring;
  (3) Misuse is subject to criminal prosecution; and
  (4) No expectation of privacy except as otherwise provided by
      applicable privacy laws.
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  Host: {{ inventory_hostname }}
  Managed by: cvadmins@utdallas.edu
ssh_sftp: internal-sftp
```

Dependencies
------------
* firewall

Testing
-------
Tests can be found in the `tests` directory. Current tests validate the following:

* yamllint
* Syntax check
* Provisioning

### Local Testing
Local testing can be done by running the `docker-compose.yml` file found in the `tests` directory

### CI Testing
CI testing configuration can be found in `.gitlab-ci.yml` in the root directory

License
-------
ISC

Author Information
------------------
CV Admins <cvadmins@utdallas.edu>
